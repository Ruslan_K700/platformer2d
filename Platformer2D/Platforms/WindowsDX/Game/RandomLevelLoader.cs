﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using System.IO;
using Microsoft.Xna.Framework.Input;

namespace Platformer2D
{
    class RandomLevelLoader : AbstractLevelLoader
    {
        private char[,] tileschar;
        private int randomInt;
        private int roomsWidth;
        private int roomsHeight;
        private bool[,] rooms;
        private List<Point> Layout = new List<Point>();
        private Point StartingRoom;
        private Point startPoint = new Point();

        enum Direction
        {
            South = 1,
            East = 2,
            North = 3,
            West = 4,
        }

        public RandomLevelLoader(Level level, ContentManager content, int levelIndex)
            : base(level, content, levelIndex)
        {
            int difficulty = levelIndex / 2;

            RandomGenerateTales(difficulty);
        }

        private bool FindPath(int x, int y, int minDistance)
        {
            if (minDistance == 0) return true;

            rooms[x, y] = true;
            Layout.Add(new Point(x, y));

            List<Direction> availablePaths = new List<Direction>();

            if (PathIsAvailable(x, y - 1, minDistance - 1)) availablePaths.Add(Direction.South);
            if (PathIsAvailable(x + 1, y, minDistance - 1)) availablePaths.Add(Direction.East);
            if (PathIsAvailable(x, y + 1, minDistance - 1)) availablePaths.Add(Direction.North);
            if (PathIsAvailable(x - 1, y, minDistance - 1)) availablePaths.Add(Direction.West);

            if (availablePaths.Count > 0)
            {
                for (int i = 0; i < availablePaths.Count; ++i)
                {
                    randomInt = random.Next(availablePaths.Count);
                    Direction randomPath = availablePaths[randomInt];

                    switch (randomPath)
                    {
                        case Direction.South:
                            if (FindPath(x, y - 1, minDistance - 1)) return true;
                            break;
                        case Direction.East:
                            if (FindPath(x + 1, y, minDistance - 1)) return true;
                            break;
                        case Direction.North:
                            if (FindPath(x, y + 1, minDistance - 1)) return true;
                            break;
                        case Direction.West:
                            if (FindPath(x - 1, y, minDistance - 1)) return true;
                            break;
                    }

                    availablePaths.Remove(randomPath);
                }
            }

            rooms[x, y] = false;
            Layout.RemoveAt(Layout.Count - 1);

            return false;
        }

        private bool PathIsAvailable(int x, int y, int minDistance)
        {
            if (x < 0 || x >= roomsWidth || y < 0 || y >= roomsHeight || (rooms[x, y] == true)) return false;
            return true;
        }

        private bool TileIsEmpty(int x, int y)
        {
            if (tileschar[x, y] == '.') return true;
            return false;
        }

        private bool TileIsPlatform(int x, int y)
        {
            if (tileschar[x, y] == '#' || tileschar[x, y] == '-' || tileschar[x, y] == '~') return true;
            return false;
        }

        private void RandomGenerateTales(int difficulty)
        {
            int width = 30 + 10 * difficulty;
            int height = 15 + 5 * difficulty;

            roomsWidth = width / 10;
            roomsHeight = height / 5;
            Level.TimeRemaining = TimeSpan.FromMinutes(1.0 + 0.2 * difficulty);

            rooms = new bool[roomsWidth, roomsHeight];

            StartingRoom.X = random.Next(0, roomsWidth);
            StartingRoom.Y = random.Next(0, roomsHeight);
            int minDistance = roomsWidth * roomsHeight / 2;

            bool generated = false;
            while (!generated)
                generated = FindPath(StartingRoom.X, StartingRoom.Y, minDistance);

            tileschar = new char[width, height];
            for (int y = 0; y < height; ++y)
            {
                for (int x = 0; x < width; ++x)
                {
                    tileschar[x, y] = '.';
                }
            }

            GeneratePlatforms(width, height);

            int numGems = minDistance * 10;
            int numEnemies = minDistance;
            int numBosses = difficulty;
            GenerateGemsAndEnemies(width, height, numGems, numEnemies, numBosses);

            LoadTiles(width, height);
            
        }

        private int GetDownPlatormDistance(int x, int y, int height)
        {
            int getDownPlatormDistance = 0;

            for (int i = 1; i < 9; ++i)
            {
                if (y + i < height && TileIsPlatform(x, y + i))
                {
                    getDownPlatormDistance = i;
                    break;
                }
            }

            return getDownPlatormDistance;
        }

        private void GeneratePlatforms(int width, int height)
        {
            Point currentRoom = new Point();
            Point nextRoom = new Point();

            for (int i = 0; i < Layout.Count; i++)
            {
                currentRoom = (i == 0) ? GetRandomRoomPoint(i) : nextRoom;

                char charTile;

                if (i != Layout.Count - 1)
                {
                    nextRoom = GetRandomRoomPoint(i + 1);

                    charTile = GetPlatformType(i);

                    if (nextRoom.X > currentRoom.X)
                    {
                        for (int i1 = 0; i1 < nextRoom.X - currentRoom.X; i1++)
                        {
                            if (TileIsEmpty(currentRoom.X + i1, currentRoom.Y))
                                tileschar[currentRoom.X + i1, currentRoom.Y] = charTile;
                        }
                    }
                    else if (nextRoom.X < currentRoom.X)
                    {
                        for (int i1 = 0; i1 < currentRoom.X - nextRoom.X; i1++)
                        {
                            if (TileIsEmpty(currentRoom.X - i1, currentRoom.Y))
                                tileschar[currentRoom.X - i1, currentRoom.Y] = charTile;
                        }
                    }
                    else
                    {
                        if (TileIsEmpty(currentRoom.X, currentRoom.Y))
                            tileschar[currentRoom.X, currentRoom.Y] = charTile;
                    }

                    if (i == 0)
                    {
                        tileschar[currentRoom.X, currentRoom.Y - 1] = '1';
                        startPoint.X = currentRoom.X;
                        startPoint.Y = currentRoom.Y - 1;
                    }
                }
                else
                {
                    tileschar[currentRoom.X, currentRoom.Y] = '#';
                    tileschar[currentRoom.X, currentRoom.Y - 1] = 'X';
                }
            }
        }

        private Point GetRandomRoomPoint(int numRoom)
        {
            Point room = new Point();
            randomInt = random.Next(0, 10);
            room.X = Layout[numRoom].X * 10 + randomInt;
            randomInt = random.Next(2, 5);
            room.Y = Layout[numRoom].Y * 5 + randomInt;

            return room;
        }

        private char GetPlatformType(int numRoom)
        {
            char charTile;
            if (numRoom == 0)
            {
                charTile = '#';
            }
            else
            {
                randomInt = random.Next(0, 2);
                if (randomInt == 0)
                    charTile = '-';
                else
                    charTile = '~';
            }

            return charTile;
        }

        private void GenerateGemsAndEnemies(int width, int height, int numGems, int numEnemies, int numBosses)
        {
            List<char> enemyTypes = new List<char>();
            enemyTypes.Add('A');
            enemyTypes.Add('B');
            enemyTypes.Add('C');
            enemyTypes.Add('D');

            for (int y = 0; y < height; ++y)
            {
                for (int x = 0; x < width; ++x)
                {
                    if (tileschar[x, y] == '.')
                    {

                        int distance = GetDownPlatormDistance(x, y, height);

                        if (distance == 1)
                        {
                            randomInt = random.Next(0, 100);
                            if (randomInt >= 0 && randomInt <= 30 && numGems > 0)
                            {
                                tileschar[x, y] = 'G';
                                numGems--;
                            }
                            else if (randomInt >= 31 && randomInt <= 40 && numEnemies > 0)
                            {
                                tileschar[x, y] = GetRandomEnemyType(enemyTypes);
                                numEnemies--;
                            }
                            else if (randomInt >= 41 && randomInt <= 45 && numBosses > 0 && Math.Abs(x - startPoint.X) > 10)
                            {
                                tileschar[x, y] = 'E';
                                numBosses--;
                            }
                        }
                        else if (distance > 1 && numGems > 0)
                        {
                            randomInt = random.Next(0, 100);
                            if (randomInt >= 0 && randomInt <= 30)
                            {
                                tileschar[x, y] = 'G';
                                numGems--;
                            }
                        }
                    }
                }
            }
        }

        private char GetRandomEnemyType(List<char> enemyTypes)
        {
            int randomEnemy = random.Next(0, enemyTypes.Count);
            return enemyTypes[randomEnemy];
        }

        private void LoadTiles(int width, int height)
        {
            Level.Tiles = new Tile[width, height];

            for (int y = 0; y < height; ++y)
            {
                for (int x = 0; x < width; ++x)
                {
                    Level.Tiles[x, y] = LoadTile(tileschar[x, y], x, y);
                }
            }
        }

    }
}
