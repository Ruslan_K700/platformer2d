﻿#region File Description
//-----------------------------------------------------------------------------
// Player.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Platformer2D
{
    class EnemyBoss : Player
    {
        public EnemyBoss(Level level, Vector2 position, string spriteSet)
            : base(level, position, spriteSet)
        {
            MoveAcceleration = 5000.0f;
            JumpLaunchVelocity = -2000.0f;
        }

        public void Update(GameTime gameTime)
        {
            GetInput();

            ApplyPhysics(gameTime);

            if (IsAlive && IsOnGround)
            {
                if (Math.Abs(Velocity.X) - 0.02f > 0)
                {
                    sprite.PlayAnimation(runAnimation);
                }
                else
                {
                    sprite.PlayAnimation(idleAnimation);
                }
            }

            // Clear input.
            movement = 0.0f;
            isJumping = false;
            isJumpingDown = false;
        }

        public void GetInput()
        {
            if (Level.Player.Position.X < Position.X)
                movement = -1.0f;
            else
                movement = 1.0f;

            if (Level.Player.Position.Y < Position.Y)
                isJumping = true;
            else if (Level.Player.Position.Y > Position.Y)
                isJumpingDown = true;
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            // Stop running when the game is paused or before turning around.
            if (!Level.Player.IsAlive ||
                Level.ReachedExit ||
                Level.TimeRemaining == TimeSpan.Zero)
            {
                sprite.PlayAnimation(idleAnimation);
            }
            else
            {
                if (Velocity.X > 0)
                    flip = SpriteEffects.FlipHorizontally;
                else if (Velocity.X < 0)
                    flip = SpriteEffects.None;

                // Draw that sprite.
                sprite.Draw(gameTime, spriteBatch, Position, flip);
            }
        }

    }
}
