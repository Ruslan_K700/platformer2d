﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using System.IO;
using Microsoft.Xna.Framework.Input;

namespace Platformer2D
{
    class FileStreamLevelLoader : AbstractLevelLoader
    {

        public FileStreamLevelLoader(Level level, ContentManager content, int levelIndex) 
            : base (level, content, levelIndex)
        {
            Level.TimeRemaining = TimeSpan.FromMinutes(2.0);

            // Load the level.
            string levelPath = string.Format("Content/Levels/{0}.txt", levelIndex);
            using (Stream fileStream = TitleContainer.OpenStream(levelPath))
                LoadTiles(fileStream);
        }

        /// <summary>
        /// Iterates over every tile in the structure file and loads its
        /// appearance and behavior. This method also validates that the
        /// file is well-formed with a player start point, exit, etc.
        /// </summary>
        /// <param name="fileStream">
        /// A stream containing the tile data.
        /// </param>
        private void LoadTiles(Stream fileStream)
        {
            // Load the level and ensure all of the lines are the same length.
            int width;
            List<string> lines = new List<string>();
            using (StreamReader reader = new StreamReader(fileStream))
            {
                string line = reader.ReadLine();
                width = line.Length;
                while (line != null)
                {
                    lines.Add(line);
                    if (line.Length != width)
                        throw new Exception(String.Format("The length of line {0} is different from all preceeding lines.", lines.Count));
                    line = reader.ReadLine();
                }
            }

            // Allocate the tile grid.
            Level.Tiles = new Tile[width, lines.Count];

            // Loop over every tile position,
            for (int y = 0; y < Level.Height; ++y)
            {
                for (int x = 0; x < Level.Width; ++x)
                {
                    // to load each tile.
                    char tileType = lines[y][x];
                    Level.Tiles[x, y] = LoadTile(tileType, x, y);
                }
            }

            // Verify that the level has a beginning and an end.
            if (Level.Player == null)
                throw new NotSupportedException("A level must have a starting point.");
            if (Level.Exit == Level.InvalidPosition)
                throw new NotSupportedException("A level must have an exit.");

        }
    }
}
